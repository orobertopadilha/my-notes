import React from 'react';
import './App.css';

import Categories from './ui/categories/Categories';
import AppHeader from './ui/header/AppHeader';
import Notes from './ui/notes/Notes';

class App extends React.Component {

    constructor() {
        super()
        this.state = {
            currentCategory: null
        }
    }

    onCategorySelected(category) {
        this.setState({ currentCategory: category })
    }

    render() {
        return (
            <div className="main-container">
                <AppHeader />
                <div className="app-body">
                    <Categories onCategorySelected={category => this.onCategorySelected(category)} />
                    <Notes category={this.state.currentCategory} />
                </div>
            </div>
        )
    }
}

export default App;
