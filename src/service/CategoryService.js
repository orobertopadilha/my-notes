import { uuid } from 'uuidv4'

const CATEGORY_LIST = 'category-list'

const save = (category) => {
    let currentList = list()
    
    currentList.push({
        id: uuid(),
        name: category
    })    

    localStorage.setItem(CATEGORY_LIST, JSON.stringify(currentList))
}

const list = () => {
    let categories = localStorage.getItem(CATEGORY_LIST)
    if (categories != null) {
        return JSON.parse(categories)
    }

    return []
}

const categoryService = { save, list } 

export default categoryService