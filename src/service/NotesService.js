import { uuid } from 'uuidv4'

const NOTE_LIST = 'notes-'

const save = (category, content) => {
    let currentList = list(category)
    
    currentList.push({
        id: uuid(),
        content: content,
        updatedAt: new Date()
    })    

    localStorage.setItem(NOTE_LIST + category, 
        JSON.stringify(currentList))
}

const list = (category) => {
    let notes = localStorage.getItem(NOTE_LIST + category)
    if (notes != null) {
        return JSON.parse(notes)
    }

    return []
}

const notesService = { save, list }

export default notesService