import './Categories.css'
import React from 'react'

import categoryService from "../../service/CategoryService";
import FormCategory from './FormCategory';

class Categories extends React.Component {

    constructor(props) {
        super(props)

        this.onCategorySelected = props.onCategorySelected
        this.state = {
            categories: []
        }
    }

    componentDidMount(){
        this.listAll()
    }

    listAll() {
        let listCategories = categoryService.list()
        this.setState({
            categories: listCategories
        })
    }

    handleSelection(category) {
        this.onCategorySelected(category)
    }

    render() {
        return (
            <div className="category-column">
                <ul>
                    {
                        this.state.categories.map( category => 
                            <li key={category.id} onClick={() => this.handleSelection(category)}>{category.name}</li>    
                        )
                    }
                    
                    <FormCategory onCategorySaved={() => this.listAll()} />

                </ul>
            </div>
        )
    }

}

export default Categories