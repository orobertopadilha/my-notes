import React from 'react'
import categoryService from "../../service/CategoryService"

class FormCategory extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            newCategory: '',
            inserting: false
        }
    }

    handleInsert() {
        if (this.state.inserting) {
            categoryService.save(this.state.newCategory)
            this.props.onCategorySaved()
        }

        this.setState({ 
            inserting: !this.state.inserting,
            newCategory: ''
        })
    }

    handleInput(event) {
        this.setState({ [event.target.name]: event.target.value})
    }

    render() {
        return (
            <li>
                { 
                    this.state.inserting && 
                    <input type="text" name="newCategory" value={this.state.newCategory} 
                        onChange={event => this.handleInput(event)} /> 
                }
                <input type="button" value="+" onClick={() => this.handleInsert()}
                    disabled={this.state.inserting && this.state.newCategory === ''}/>
            </li>
        )
    }

}


export default FormCategory