import './AppHeader.css'

const AppHeader = () => {

    return (
        <div className="header">
            <h1>My Notes</h1>
        </div>
    )
}

export default AppHeader