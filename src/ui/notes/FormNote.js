import './Notes.css'
import React from 'react'
import notesService from '../../service/NotesService'

class FormNote extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            newNote: ''
        }
    }

    save() {
        notesService.save(this.props.category.id, this.state.newNote)
        this.setState({ newNote : '' })
        
        this.props.onNoteSaved()
    }

    handleInput(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    render() {
        return (
            <div className="note-card">
                <textarea rows="3" name="newNote" onChange={(event) => this.handleInput(event)} value={this.state.newNote}></textarea>
                <input type="button" value="Salvar" onClick={() => this.save()} disabled={this.state.newNote === ''}/>
            </div>
        )
    }

}

export default FormNote