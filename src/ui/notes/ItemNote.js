import './Notes.css'

const ItemNote = ({ note }) => {

    return (
        <div key={note.id} className="note-card">
            {note.content}
            <br />
            <span>{new Date(note.updatedAt).toLocaleDateString()}</span>
        </div>
    )

}

export default ItemNote