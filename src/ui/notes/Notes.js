import './Notes.css'
import React from 'react'
import notesService from '../../service/NotesService'
import FormNote from './FormNote'
import ItemNote from './ItemNote'

class Notes extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            notes: []
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.category == null || (prevProps.category.id !== this.props.category.id)) {
            this.listNotes()
        }
    }

    listNotes() {
        let allNotes = notesService.list(this.props.category.id)
        this.setState({
            notes: allNotes
        })
    }

    render() {
        return (
            this.props.category != null ?
                <div className="notes-column">
                    <h1>{this.props.category.name}</h1>

                    <FormNote category={this.props.category} onNoteSaved={() => this.listNotes()}/>

                    {
                        this.state.notes.map( note => <ItemNote note={note} /> )
                    }
                    
                </div>
            :
                <></>
        )
    }

}

export default Notes